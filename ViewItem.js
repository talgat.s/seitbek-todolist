import ViewUpdateItem from "./ViewUpdateItem.js";

class ViewItem {
  constructor(todoList) {
    this.todoList = todoList;

    this.viewUpdateItem = new ViewUpdateItem(todoList);
  }

  init() {
    this.viewUpdateItem.init?.();
  }

  updateHandler = ({ currentTarget }) => {
    const { dataset } = currentTarget;

    this.viewUpdateItem.open(Number(dataset?.ind));
  };

  deleteHandler = ({ currentTarget }) => {
    const { dataset } = currentTarget;

    this.todoList.deleteItem(Number(dataset?.ind));
  };

  render = (text, ind, signal) => {
    const p = document.createElement("p");
    p.innerText = text;

    const updateButton = document.createElement("button");
    updateButton.innerText = "Update";
    updateButton.setAttribute("data-ind", ind);
    updateButton.addEventListener("click", this.updateHandler, { signal });

    const deleteButton = document.createElement("button");
    deleteButton.innerText = "Delete";
    deleteButton.setAttribute("data-ind", ind);
    deleteButton.addEventListener("click", this.deleteHandler, { signal });

    const li = document.createElement("li");
    li.append(p, updateButton, deleteButton);

    return li;
  };
}

export default ViewItem;
