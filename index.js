import View from "./View.js";
import TodoList from "./TodoList.js";

const todoList = new TodoList();
const view = new View(todoList);
todoList.addRenderer(view.render);
view.init();

// const wait = (delay) =>
//   new Promise((res) => {
//     setTimeout(res, delay);
//   });

// async function main() {
//   await wait(1000);
//   todoList.addItem("1");
//   todoList.addItem("2");
//   todoList.addItem("3");
//   todoList.addItem("4");
//
//   await wait(1000);
//   todoList.updateItem(1, "10");
//
//   await wait(1000);
//   todoList.deleteItem(0);
// }
//
// main();
