import ViewItem from "./ViewItem.js";
import ViewText from "./ViewText.js";
import ViewClear from "./ViewClear.js";

class View {
  constructor(todoList) {
    this.itemController = new AbortController();
    this.listEl = document.getElementById("todo-list");

    this.viewItem = new ViewItem(todoList);
    this.viewText = new ViewText(todoList);
    this.viewClear = new ViewClear(todoList);
  }

  init = () => {
    this.viewItem.init?.();
    this.viewText.init?.();
    this.viewClear.init?.();
  };

  render = (list) => {
    this.itemController.abort("generate new list");
    this.itemController = new AbortController();
    const signal = this.itemController.signal;

    const items = list.map((item, ind) =>
      this.viewItem.render(item, ind, signal)
    );

    this.listEl.replaceChildren(...items);
  };
}

export default View;
