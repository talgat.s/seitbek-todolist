class ViewUpdateItem {
  constructor(todoList) {
    this.todoList = todoList;

    this.dialog = document.getElementById("update-dialog");
    this.form = document.getElementById("update-todo-form");
  }

  open = (ind) => {
    this.dialog.showModal();

    this.form.setAttribute("data-ind", ind);
    const text = this.todoList.list[ind];
    const textEl = this.form.elements["text"];
    textEl.value = text;
  };

  init = () => {
    this.form.addEventListener("submit", this.submitHandler);
  };

  submitHandler = (event) => {
    event.preventDefault();

    const { currentTarget } = event;
    const ind = Number(currentTarget.dataset.ind);
    const form = new FormData(currentTarget);

    this.todoList.updateItem(ind, form.get("text"));
    currentTarget.reset();
    this.dialog.close();
  };
}

export default ViewUpdateItem;
