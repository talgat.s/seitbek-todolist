class ViewText {
  constructor(todoList) {
    this.todoList = todoList;

    this.form = document.getElementById("add-todo-form");
  }

  init = () => {
    this.form.addEventListener("submit", this.submitHanlder);
  };

  submitHanlder = (event) => {
    event.preventDefault();

    const { currentTarget } = event;
    const form = new FormData(currentTarget);

    this.todoList.addItem(form.get("text"));
    currentTarget.reset();
  };
}

export default ViewText;
