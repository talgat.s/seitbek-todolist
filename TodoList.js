class TodoList {
  constructor() {
    // [String]
    this.list = [];
    this.renderList = null;
  }

  addRenderer(renderList) {
    this.renderList = renderList;
  }

  addItem(text) {
    this.list.push(text);

    this.renderList?.(this.list);
  }

  updateItem(ind, text) {
    this.list[ind] = text;

    this.renderList?.(this.list);
  }

  deleteItem(ind) {
    this.list.splice(ind, 1);

    this.renderList?.(this.list);
  }

  deleteList() {
    this.list = [];

    this.renderList?.(this.list);
  }
}

export default TodoList;
