class ViewClear {
  constructor(todoList) {
    this.todoList = todoList;

    this.button = document.getElementById("list-clear");
  }

  init = () => {
    this.button.addEventListener("click", this.clickHandler);
  };

  clickHandler = () => {
    this.todoList.deleteList();
  };
}

export default ViewClear;
